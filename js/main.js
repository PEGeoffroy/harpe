import { Map } from "./class/map.js";
import { Tile } from "./class/tile.js";
import { Camera } from "./class/camera.js";
import { Character } from "./class/character.js";

'use strict';

let harpeMap = new Map(20, 20);
harpeMap.init();
harpeMap.grounds[4][3] =new Tile("wall", true);
harpeMap.creatures[2][1] = new Character("PE");
harpeMap.camera.displayMap(harpeMap);
//console.log(harpeMap.creatures[2][1].name);
//let harpeCamera = new Camera();
//harpeCamera.displayMap(harpeMap);

// testMap.createMap();
// testMap.debugMap();

 document.addEventListener('keyup', function(event) {
   let keyTouch = event.key;
     harpeMap.isObstacle(keyTouch);
     harpeMap.camera.displayMap(harpeMap);
   //testMap.createMap();
 });