import { Character } from "./character.js";

export class Camera {
  constructor(radius = 4, center = [2, 1]) {
    this.radius = radius;
    this.size = (this.radius * 2) + 1;
    this.center = center;
  }

  displayMap(map) {

    let mapGrounds = document.querySelector("#grounds");
    let mapCreatures = document.querySelector("#creatures");
    mapGrounds.innerHTML = "";
    mapCreatures.innerHTML = "";

    let xBorder = this.center[0] - this.radius;
    let yBorder = this.center[1] - this.radius;

    // let w = x + this.size;
    // let h = x + this.size;

    for (let x = 0; x < this.size; x++) {
      let rowGrounds = document.createElement("tr");
      let rowCreatures = document.createElement("tr");
      rowGrounds.className = `rowGrounds${x}`;
      rowCreatures.className = `rowGrounds${x}`;

      for (let y = 0; y < this.size; y++) {
        let cellGrounds = document.createElement("td");
        let cellCreatures = document.createElement("td");
        cellGrounds.className = `cellGrounds${y}`;
        cellCreatures.className = `cellGrounds${y}`;

        let xMap = x + xBorder;
        let yMap = y + yBorder;

        if (xMap < 0 || yMap < 0 || xMap > map.width - 1 || yMap > map.height - 1) {
          cellGrounds.className += " void";
          cellCreatures.className += " void";
        } else if (x === 0 && y === 0 || x === 1 && y === 0 || x === 0 && y === 1 || x === 0 && y === this.size - 1 || x === this.size - 1 && y === 0 || x === this.size - 1 && y === this.size - 1) {
        
        } else {
          let tileGrounds = map.grounds[xMap][yMap].type;
          if (tileGrounds === "wall") {
            cellGrounds.className += " cube";
          } else if (tileGrounds === "step") {
            cellGrounds.className += " step";
          } else if (tileGrounds === "floor") {
            cellGrounds.className += " floor tile";
          } else if (tileGrounds === "start") {
            cellGrounds.className += " start";
          }
          //console.log(`${xMap} / ${yMap}`);
          if (map.creatures[xMap][yMap] instanceof Character){
            cellCreatures.className += " player";
          }
          //console.log(map.creatures[xMap][yMap]);
        }
        rowGrounds.appendChild(cellGrounds);
        rowCreatures.appendChild(cellCreatures);
      }
      mapGrounds.appendChild(rowGrounds);
      mapCreatures.appendChild(rowCreatures);
    }
  }
}